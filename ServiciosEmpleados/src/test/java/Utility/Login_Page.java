package Utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Login_Page {

    public static void login(WebDriver driver, String username, String password) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Constant_value_utility.userInput)));
        driver.findElement(By.id(Constant_value_utility.userInput)).sendKeys(username);
        driver.findElement(By.id(Constant_value_utility.passInput)).sendKeys(password);
        driver.findElement(By.id(Constant_value_utility.btnIniciarSesion)).click();
    }
}