package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DatosPersonales {
    private static WebElement element = null;


    // Título de la pantalla de Datos personales del empleado
    public static WebElement tituloPantalla(WebDriver driver) {
        element = driver.findElement(By.xpath("//h1[text()='Datos Personales']"));
        return element;
    }

    // Título de la pantalla de Datos Bancarios
    public static WebElement tituloPantallaDatosBancarios(WebDriver driver) {
        element = driver.findElement(By.xpath("//h1[text()='Datos bancarios']"));
        return element;
    }

    // Título de la pantalla de Datos Administrativos
    public static WebElement tituloPantallaDatosAdministrativos(WebDriver driver) {
        element = driver.findElement(By.xpath("//h1[text()='Datos administrativos']"));
        return element;
    }

    // Datos de la sección de importante de la pantalla de Datos del empleado
    public static WebElement tituloSeccionImportante(WebDriver driver) {
        element = driver.findElement(By.xpath("//h2[text()='Importante']"));
        return element;
    }

    // Contenido de la sección importante
    public static WebElement contenidoSeccionImportante(WebDriver driver) {
        element = driver.findElement(By.xpath("//h2[text()='Importante']/ancestor::span/following-sibling::span[1]"));
        return element;
    }

    // Botón solicitar modificación en Sección Importante
    public static WebElement btnSolicitarModificacion(WebDriver driver) {
        element = driver.findElement(By.xpath("(//span[text()='Modificar'][1]/ancestor::a)[1]"));
        return element;
    }
/* Datos básicos*/
    // Título sección Datos básicos
    public static WebElement tituloSeccionDatosBasicos(WebDriver driver) {
        element = driver.findElement(By.xpath("//h2[text()='Datos Básicos']"));
        return element;
    }

    public static WebElement seccionDatosBasicos(WebDriver driver) {
        element = driver.findElement(By.xpath("//h2[text()='Datos Básicos']/ancestor::span/following-sibling::span"));
        return element;
    }

    // Campo número de identificación fiscal
    public static WebElement numIdentificacionFiscal(WebDriver driver) {
        element = driver.findElement(By.xpath("//label[text()='Documento de Identificación fiscal']/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo nombre y apellidos
    public static WebElement nombreApellidos(WebDriver driver) {
        element = driver.findElement(By.xpath("//label[text()='Nombre y apellidos']/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo sexo
    public static WebElement sexo(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Sexo'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo estado civil
    public static WebElement estadoCivil(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Estado civil'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    /* Datos de nacimiento */
    // Título de la sección de Datos de nacimiento
    public static WebElement tituloDatosNacimiento(WebDriver driver) {
        element = driver.findElement(By.xpath("//span[@class='help-lg' and text()='Datos de nacimiento']"));
        return element;
    }

    // Campo fecha de nacimiento
    public static WebElement fechaNacimiento(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Fecha nacimiento'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo País
    public static WebElement pais(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='País'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo Provincia
    public static WebElement provincia(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Provincia'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo Localidad
    public static WebElement localidad(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Localidad'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo nacionalidad
    public static WebElement nacionalidad(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Nacionalidad'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    /* Datos del domicilio */

    // Título de la sección de Datos del domicilio
    public static WebElement tituloDatosDomicilio(WebDriver driver) {
        element = driver.findElement(By.xpath("//span[@class='help-lg' and text()='Datos del domicilio']"));
        return element;
    }
    // Campo pais
    public static WebElement paisDomicilio(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='País'])[5]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo provincia
    public static WebElement provinciaDomicilio(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Provincia'])[5]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo localidad
    public static WebElement localidadDomicilio(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Localidad'])[5]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo direccionCompleta
    public static WebElement direccionCompleta(WebDriver driver) {
        element = driver.findElement(By.xpath("//label[text()='Dirección completa']/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo codigoPostal
    public static WebElement codigoPostal(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Código postal'])[1]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo telefono
    public static WebElement telefono(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Teléfono'])[2]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    /* Otros Datos de contacto*/

    // Título sección Otros datos de contacto
    public static WebElement tituloOtrosDatos(WebDriver driver) {
        element = driver.findElement(By.xpath("//span[@class='help-lg' and text()='Otros datos de contacto']"));
        return element;
    }

    // Mensaje sección Datos básicos
    public static WebElement mensajeSeccionOtrosDatosContacto(WebDriver driver) {
        element = driver.findElement(By.xpath("//span[@class='help-lg' and text()='Otros datos de contacto']/following-sibling::span"));
        return element;
    }

    // Campo móvil
    public static WebElement movil(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Móvil'])[1]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo correo electrónico
    public static WebElement correoElectronico(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Correo electrónico'])[1]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Campo correo electrónico secundario
    public static WebElement correoElectronicoSecundario(WebDriver driver) {
        element = driver.findElement(By.xpath("(//label[text()='Correo electrónico secundario'])[1]/ancestor::tr[1]/following-sibling::tr"));
        return element;
    }

    // Imagen usuario
    public static WebElement imagenUsuario(WebDriver driver) {
        element = driver.findElement(By.xpath("//img[contains(@class,'img-foto')][3]"));
        return element;
    }
}
