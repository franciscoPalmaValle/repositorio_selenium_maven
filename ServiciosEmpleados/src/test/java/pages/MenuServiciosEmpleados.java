package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MenuServiciosEmpleados {

    private static WebElement element = null;

    // Enlace del menú que nos redirige a la pantalla de Datos personales
    public static WebElement btnMenuDatosPersonales(WebDriver driver) {
        element = driver.findElement(By.xpath("//a[@aria-label='Datos personales']/ancestor::h1"));
        return element;
    }

    // Enlace del submenú de Datos personales que nos redirige a la pantalla de Datos personales del empleado
    public static WebElement linkDatosPersonales(WebDriver driver) {
        element = driver.findElement(By.xpath("//a[contains(text(), 'Datos personales proveedor')]"));
        return element;
    }

    // Enlace del submenú de Datos personales que nos redirige a la pantalla de Datos personales del empleado
    public static WebElement linkDatosBancarios(WebDriver driver) {
        element = driver.findElement(By.linkText("Datos bancarios proveedor"));
        return element;
    }

    // Enlace del submenú de Datos personales que nos redirige a la pantalla de Datos personales del empleado
    public static WebElement linkDocumentacion(WebDriver driver) {
        element = driver.findElement(By.linkText("Documentación"));
        return element;
    }
}
