package Utility;

public class User {
    public String name;
    public String pass;
    public String nombreRazon;
    public String dni;
    public String direccion;
    public String nombreVia;
    public String numero;
    public String escalera;
    public String piso;
    public String puerta;
    public String codPostal;
    public String localidad;
    public String provincia;
    public String pais;
    public String telefono;
    public String correoPersonal;
    public String fax;
}
