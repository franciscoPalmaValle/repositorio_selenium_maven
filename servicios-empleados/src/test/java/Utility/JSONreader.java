package Utility;

import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;

import java.io.FileReader;


public class JSONreader {
    String[] arrayArguments = {
            "name",
            "pass",
            "nombreRazon",
            "dni",
            "direccion",
            "nombreVia",
            "numero",
            "escalera",
            "piso",
            "puerta",
            "codPostal",
            "localidad",
            "provincia",
            "pais",
            "telefono",
            "correoPersonal",
            "fax"
    };

        public static User getUsuario(String user) {
            try {
                JSONParser parser = new JSONParser();
                User usuario = null;
                Object obj = parser.parse(new FileReader("src/test/java/resources/datos.json"));
                JSONObject users = (JSONObject) obj;
                JSONObject user1 = (JSONObject) users.get(user);
                usuario = new User();
                usuario.name = (String) user1.get("name");
                usuario.pass = (String) user1.get("pass");
                usuario.nombreRazon = (String) user1.get("nombreRazon");
                usuario.dni = (String) user1.get("dni");
                usuario.direccion = (String) user1.get("direccion");
                usuario.nombreVia = (String) user1.get("nombreVia");
                usuario.escalera = (String) user1.get("escalera");
                usuario.piso = (String) user1.get("piso");
                usuario.puerta = (String) user1.get("puerta");
                usuario.codPostal = (String) user1.get("codPostal");
                usuario.localidad = (String) user1.get("localidad");
                usuario.provincia = (String) user1.get("provincia");
                usuario.pais = (String) user1.get("pais");
                usuario.telefono = (String) user1.get("telefono");
                usuario.correoPersonal = (String) user1.get("correoPersonal");
                usuario.fax = (String) user1.get("fax");
                System.out.println("name: " + usuario.name);
                return usuario;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
