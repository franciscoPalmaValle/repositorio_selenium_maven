package test;

import Utility.Constant_value_utility;
import Utility.JSONreader;
import Utility.Login_Page;
import Utility.User;

import environment.EnvironmentManager;
import environment.RunEnvironment;

// Imports JUnit
import org.junit.jupiter.api.*;

// Imports Java
import java.util.concurrent.TimeUnit;

// Imports selenium
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Pruebas del portal del empleado")
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class ServiciosEmpleados {

    @BeforeEach
    public void startBrowser() {
        EnvironmentManager.initWebDriver();
        WebDriver driver = RunEnvironment.getWebDriver();
        driver.manage().window().maximize();
        driver.get(Constant_value_utility.URL);
        System.out.println("Before each");
    }

    @AfterEach
    public void setDown() {
        EnvironmentManager.shutDownDriver();
        System.out.println("After each");
    }

    @DisplayName("Pruebas datos personales")
    @ParameterizedTest
    @ValueSource(strings = { "user1" })
    void test1(String user) {
        try {
            // Get driver
            WebDriver driver = RunEnvironment.getWebDriver();

            // Instanciamos la espera y esperamos que se sea visible el botón del menú de datos personales
            User usuario = JSONreader.getUsuario(user);
            assertNotNull(usuario);
            System.out.println("Usuario 1: " +usuario.name);
            Login_Page.login(driver, usuario.name, usuario.pass);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            // Clicamos en el botón
            pages.MenuServiciosEmpleados.btnMenuDatosPersonales(driver).click();

            // Clicamos en el link que aparece
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.visibilityOf(pages.MenuServiciosEmpleados.linkDatosPersonales(driver)));
            pages.MenuServiciosEmpleados.linkDatosPersonales(driver).click();

            // Comprobamos el título de la pantalla
            assertTrue(pages.DatosPersonales.tituloPantalla(driver).isDisplayed());
            assertEquals(pages.DatosPersonales.tituloPantalla(driver).getText(), "Datos Personales");

            // Comprobamos la sección de Importante
            assertTrue(pages.DatosPersonales.tituloSeccionImportante(driver).isDisplayed());
            assertEquals(pages.DatosPersonales.tituloSeccionImportante(driver).getText(), "Importante");
            assertTrue(pages.DatosPersonales.contenidoSeccionImportante(driver).isDisplayed());
            assertTrue(pages.DatosPersonales.btnSolicitarModificacion(driver).isDisplayed());

            // Comprobamos la sección de Datos básicos
            assertTrue(pages.DatosPersonales.tituloSeccionDatosBasicos(driver).isDisplayed());
            assertEquals(pages.DatosPersonales.tituloSeccionDatosBasicos(driver).getText(), "Datos Básicos");
            assertTrue(pages.DatosPersonales.seccionDatosBasicos(driver).isDisplayed());
            assertEquals(pages.DatosPersonales.seccionDatosBasicos(driver).findElements(By.tagName("label")).get(0).getText(), "Nombre y apellidos/Razón social");
            assertEquals(pages.DatosPersonales.seccionDatosBasicos(driver).findElements(By.tagName("label")).get(1).getText(), "Documento de Identificación fiscal");
            assertEquals(pages.DatosPersonales.numIdentificacionFiscal(driver).getText(), usuario.dni);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
