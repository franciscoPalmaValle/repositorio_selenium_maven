package environment;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

public class EnvironmentManager {

    public static void initWebDriver() {
        /*
        // Set Chrome Options
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--ignore-certificate-errors");

        // Set proxy
        String PROXY = "localhost:9200";
        Proxy proxy = new Proxy();
        proxy.setHttpProxy(PROXY);
        proxy.setFtpProxy(PROXY);
        proxy.setSslProxy(PROXY);
*/
        // Set driver and capabilities
        System.setProperty("webdriver.chrome.driver", "src/test/java/resources/chromedriver.exe");
        /*
        options.setCapability("proxy", proxy);
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        ChromeDriver driver = new ChromeDriver(options);
        */
        ChromeDriver driver = new ChromeDriver();
        RunEnvironment.setWebDriver(driver);
    }

    public static void shutDownDriver() {
        RunEnvironment.getWebDriver().quit();
    }
}
