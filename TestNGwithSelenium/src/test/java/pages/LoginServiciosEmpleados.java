package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginServiciosEmpleados {
    private static WebElement element = null;

    public static void accesoServiciosEmpleado(WebDriver driver, String baseURL) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            driver.get(baseURL);
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("app-name"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static WebElement inputUsuario(WebDriver driver) {
        element = driver.findElement(By.id("username"));
        return element;
    }

    public static WebElement inputPass(WebDriver driver) {
        element = driver.findElement(By.id("password"));
        return element;
    }

    public static WebElement btnIniciarSesion(WebDriver driver) {
        element = driver.findElement(By.className("btn-submit"));
        return element;
    }
}
