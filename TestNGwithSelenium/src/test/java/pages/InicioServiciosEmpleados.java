package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InicioServiciosEmpleados {
    private static WebElement element = null;

    public static WebElement btnAyuda(WebDriver driver) {
        element = driver.findElements(By.xpath("//td[@class='af_menuBar_item']")).get(0);
        return element;
    }

    public static WebElement btnIdioma(WebDriver driver) {
        element = driver.findElements(By.xpath("//td[@class='af_menuBar_item']")).get(1);
        return element;
    }

    public static WebElement btnUsuario(WebDriver driver) {
        element = driver.findElement(By.xpath("//table[@class='af_menuBar_items']"));
        return element;
    }
}
