package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import pages.InicioServiciosEmpleados;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class ServiciosEmpleado {

    private static WebDriver driver = null;
    /*
    private static final String RUTA_CSV_USUARIOS = "/src/resources/usuarios.csv";
    private static final String RUTA_CSV_DATOS_BASICOS = "/resources/datosBasicos.csv";
    private static final String RUTA_CSV_DATOS_NACIMIENTO = "/resources/datosNacimiento.csv";
    private static final String RUTA_CSV_DATOS_DOMICILIO = "/resources/datosDomicilio.csv";
    private static final String RUTA_CSV_OTROS_DATOS = "/resources/otrosDatosContacto.csv";
    */
    private static String baseURL = "https://portal.ocu.es/ServiciosApp/faces/inicioServicios";
    private String user = "francisco.palma.v";
    private String pass = "Agosto2018";

    @BeforeMethod
    static void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/java/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pages.LoginServiciosEmpleados.accesoServiciosEmpleado(driver, baseURL);
    }

    @Test
    public void loginUsuario() {
        try {
            pages.LoginServiciosEmpleados.inputUsuario(driver).sendKeys(user);
            pages.LoginServiciosEmpleados.inputPass(driver).sendKeys(pass);
            pages.LoginServiciosEmpleados.btnIniciarSesion(driver).click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            assertTrue(InicioServiciosEmpleados.btnUsuario(driver).isDisplayed());
            assertTrue(InicioServiciosEmpleados.btnUsuario(driver).getText().contains(user));
            assertTrue(InicioServiciosEmpleados.btnAyuda(driver).isDisplayed());
            assert (InicioServiciosEmpleados.btnAyuda(driver).getText()).equals("¿Necesitas ayuda?");
            assertTrue(InicioServiciosEmpleados.btnIdioma(driver).isDisplayed());
            assert (InicioServiciosEmpleados.btnIdioma(driver).getText()).equals("Cambiar idioma");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * @Test void Test2() { try { WebDriverWait wait = new WebDriverWait(driver,
     * 10); pages.DatosPersonales.btnMenuDatosPersonales(driver).click();
     * wait.until(ExpectedConditions.visibilityOf(pages.DatosPersonales.
     * linkDatosPersonalesEmpleado(driver)));
     * pages.DatosPersonales.linkDatosPersonalesEmpleado(driver).click();
     * assertTrue(pages.DatosPersonales.tituloPantalla(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.tituloPantalla(driver).getText(),
     * "Datos personales");
     * assertTrue(pages.DatosPersonales.tituloSeccionImportante(driver).isDisplayed(
     * ));
     * assertEquals(pages.DatosPersonales.tituloSeccionImportante(driver).getText(),
     * "Importante");
     * assertTrue(pages.DatosPersonales.contenidoSeccionImportante(driver).
     * isDisplayed());
     * assertEquals(pages.DatosPersonales.contenidoSeccionImportante(driver).getText
     * (),
     * "En esta pantalla podrás consultar tus datos personales. Para modificarlos pulsa en 'Solicitar modificación'.");
     * 
     * 
     * 
     * 
     * assertTrue(pages.DatosPersonales.btnSolicitarModificacion(driver).isDisplayed
     * ()); } catch (Exception e) { e.printStackTrace(); } }
     * 
     * @Test void Test3(String dni, String nombre, String sexo, String estadoCivil)
     * { try { assertTrue(pages.DatosPersonales.tituloSeccionDatosBasicos(driver).
     * isDisplayed());
     * assertEquals(pages.DatosPersonales.tituloSeccionDatosBasicos(driver).getText(
     * ), "Datos básicos");
     * assertTrue(pages.DatosPersonales.mensajeSeccionDatosBasicos(driver).
     * isDisplayed());
     * assertEquals(pages.DatosPersonales.mensajeSeccionDatosBasicos(driver).getText
     * (),
     * "Algunos datos no pueden ser modificados desde esta página, así que si encuentras algún error o necesitas modificar alguno, por favor dirígite a la secretaría de tu Universidad.");
     * 
     * 
     * 
     * 
     * assertTrue(pages.DatosPersonales.numIdentificacionFiscal(driver).isDisplayed(
     * ));
     * assertEquals(pages.DatosPersonales.numIdentificacionFiscal(driver).getText(),
     * dni);
     * assertTrue(pages.DatosPersonales.nombreApellidos(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.nombreApellidos(driver).getText(),
     * nombre); assertTrue(pages.DatosPersonales.sexo(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.sexo(driver).getText(), sexo);
     * assertTrue(pages.DatosPersonales.estadoCivil(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.estadoCivil(driver).getText(),
     * estadoCivil);
     * assertTrue(pages.DatosPersonales.imagenUsuario(driver).isDisplayed()); }
     * catch (Exception e) { e.printStackTrace(); } }
     * 
     * @Test void Test4(String datNacimiento, String pais, String provincia, String
     * localidad, String nacionalidad) { try {
     * assertTrue(pages.DatosPersonales.fechaNacimiento(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.fechaNacimiento(driver).getText(),
     * datNacimiento); assertTrue(pages.DatosPersonales.pais(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.pais(driver).getText(), pais);
     * assertTrue(pages.DatosPersonales.provincia(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.provincia(driver).getText(), provincia);
     * assertTrue(pages.DatosPersonales.localidad(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.localidad(driver).getText(), localidad);
     * assertTrue(pages.DatosPersonales.nacionalidad(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.nacionalidad(driver).getText(),
     * nacionalidad); } catch (Exception e) { e.printStackTrace(); } }
     * 
     * @Test void Test5(String paisDomicilio, String provinciaDomicilio, String
     * localidadDomicilio, String direccionCompleta, String codigoPostal, String
     * telefono) { try {
     * assertTrue(pages.DatosPersonales.paisDomicilio(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.paisDomicilio(driver).getText(),
     * paisDomicilio);
     * assertTrue(pages.DatosPersonales.provinciaDomicilio(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.provinciaDomicilio(driver).getText(),
     * provinciaDomicilio);
     * assertTrue(pages.DatosPersonales.localidadDomicilio(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.localidadDomicilio(driver).getText(),
     * localidadDomicilio);
     * assertTrue(pages.DatosPersonales.direccionCompleta(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.direccionCompleta(driver).getText(),
     * direccionCompleta);
     * assertTrue(pages.DatosPersonales.codigoPostal(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.codigoPostal(driver).getText(),
     * codigoPostal);
     * assertTrue(pages.DatosPersonales.telefono(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.telefono(driver).getText(), telefono); }
     * catch (Exception e) { e.printStackTrace(); } }
     * 
     * @Test void Test6(String movil, String correo, String otroCorreo) { try {
     * assertTrue(pages.DatosPersonales.movil(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.movil(driver).getText(), movil);
     * assertTrue(pages.DatosPersonales.correoElectronico(driver).isDisplayed());
     * assertEquals(pages.DatosPersonales.correoElectronico(driver).getText(),
     * correo);
     * assertTrue(pages.DatosPersonales.correoElectronicoSecundario(driver).
     * isDisplayed());
     * assertEquals(pages.DatosPersonales.correoElectronicoSecundario(driver).
     * getText(), otroCorreo); } catch (Exception e) { e.printStackTrace(); } }
     * 
     * void Test7() { pages.DatosPersonales.linkDatosBancarios(driver).click();
     * assertTrue(pages.DatosPersonales.tituloPantallaDatosBancarios(driver).
     * isDisplayed());
     * assertEquals(pages.DatosPersonales.tituloPantallaDatosBancarios(driver).
     * getText(), "Datos bancarios");
     * pages.DatosPersonales.linkDatosAdministrativos(driver).click();
     * assertTrue(pages.DatosPersonales.tituloPantallaDatosAdministrativos(driver).
     * isDisplayed());
     * assertEquals(DatosPersonales.tituloPantallaDatosAdministrativos(driver).
     * getText(), "Datos administrativos"); }
     */

    @AfterMethod
    static void tearDown() {
        driver.quit();
    }
}